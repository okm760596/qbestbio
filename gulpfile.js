var gulp = require('gulp');
var scss = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync');
var nodemon = require('gulp-nodemon');

const fileinclude = require('gulp-file-include');

 var concat = require('gulp-concat');
 var uglify = require('gulp-uglify');
 var rename = require('gulp-rename');
 var del = require('del');
 var changed = require('gulp-changed');
 var fileSync = require('gulp-file-sync');

// ❗ 하위뎁스 컴파일 잘안됨. file include 쪽 문제잇음.


var PATH = {
  HTML: './workspace/html',
  INC: './workspace/html/include',
  GUIDE: './workspace/asset/guide',
  ASSETS: {
    FONTS: './workspace/asset/fonts',
    IMAGES: './workspace/asset/img',
    STYLE: './workspace/asset/style',
    SCRIPT: './workspace/asset/js',
    SCRIPTMERGE: './workspace/asset/scriptmerge'
  }
},
DEST_PATH = {
  HTML: './dist/',
  INC: './dist/include',
  GUIDE: './dist/asset/guide',
  ASSETS: {
    FONTS: './dist/asset/fonts',
    IMAGES: './dist/asset/img',
    STYLE: './dist/asset/css',
    SCRIPT: './dist/asset/js'
  }
}

const fileIncludeOpt ={
  prefix: '@@',
  basepath: '@file',
  context:{
    'webRoot' : ".",
    'htmlRoot' : '..',
    'imageRoot' : '../asset/img',
    'cssRoot': '../asset/css',
  }
}

const browsSyncOpt = {
  stream: true
}

gulp.task('clear', () => {
  return new Promise( resolve => {
    del.sync(['./dist/']);
    resolve();
  });
});


// gulp.task('librarySync', () => {
//   return new Promise( resolve => {
//     fileSync(PATH.ASSETS.LIB, DEST_PATH.ASSETS.LIB, {recursive:true});

//     gulp.src(PATH.ASSETS.FONTS + '/*.*')
//       .pipe( browserSync.reload({stream:true}));

//     resolve();
//   });
// });

gulp.task('script:copy', () => {
  return new Promise( resolve => {
    gulp.src([
      PATH.ASSETS.SCRIPT + '/**/*.*'
      ])
      .pipe( gulp.dest(DEST_PATH.ASSETS.SCRIPT))
      .pipe( browserSync.reload({stream:true}));
      resolve();
  });
});

gulp.task('fontsSync', () => {
  return new Promise( resolve => {
    fileSync(PATH.ASSETS.FONTS, DEST_PATH.ASSETS.FONTS, {recursive:true});

    gulp.src(PATH.ASSETS.FONTS + '/*.*')
      .pipe( browserSync.reload({stream:true}));
      
    resolve();
  });
});

gulp.task('imagesSync', () =>{
  return new Promise( resolve => {

    fileSync(PATH.ASSETS.IMAGES,DEST_PATH.ASSETS.IMAGES,{recursive:true});
    
    gulp.src(PATH.ASSETS.IMAGES + '/**/*.*')
      .pipe( browserSync.reload({stream:true}));

    resolve();
  });
});

gulp.task('nodemon:start', () => {
  return new Promise( resolve => {
    nodemon({
      script: 'app.js',
      watch: 'app'
    });
  
    resolve();
  });
});

// script merge
gulp.task('script:bulid', () => {
  return new Promise( resolve => {
    gulp.src([
        // '!' + PATH.ASSETS.SCRIPTMERGE + '/_*.js'
        PATH.ASSETS.SCRIPTMERGE + '/test.js'
      ])
      .pipe( concat('common.js') )
      .pipe( gulp.dest(DEST_PATH.ASSETS.SCRIPT) )
      // .pipe( uglify({
      //   mangle: true
      // }) )
      // .pipe( rename('common.min.js') )
      // .pipe( gulp.dest(DEST_PATH.ASSETS.SCRIPT) )
      .pipe( browserSync.reload({stream:true}));

      resolve();
  });
});

gulp.task( 'scss:compile', () => {
  return new Promise( resolve => {
    var options = {
      outputStyle: "indented",
      // indentType: "space",
      // indentWidth: 2
    };

    gulp.src(PATH.ASSETS.STYLE + '/**/*.scss')
      .pipe(sourcemaps.init())
      .pipe(scss.sync().on('error', scss.logError))
      .pipe(scss(options))
      .pipe(sourcemaps.write('./maps'))
      .pipe(gulp.dest(DEST_PATH.ASSETS.STYLE))
      .pipe(browserSync.reload({stream:true}));
    
    resolve();
  });
});

gulp.task( 'scssguide:compile', () => {
  return new Promise( resolve => {
    var options = {
      outputStyle: "compressed",
    };

    gulp.src(PATH.HTML + '/guide/*.scss')
      .pipe(sourcemaps.init())
      .pipe(scss.sync().on('error', scss.logError))
      .pipe(scss(options))
      .pipe(sourcemaps.write('./maps'))
      .pipe(gulp.dest(DEST_PATH.HTML + '/guide'))
      .pipe(browserSync.reload({stream:true}));
    
    resolve();
  });
});

gulp.task('html', () => {
  return new Promise( resolve => {
    // try{
    gulp.src([
    PATH.HTML + '/**/*.*',
    '!'+PATH.HTML + '/include/*.html'
    // PATH.HTML + '/plan_manage/*.html'
    ])
    // .pipe(changed(DEST_PATH.HTML))
    .pipe( fileinclude(fileIncludeOpt) )
    .pipe(gulp.dest(DEST_PATH.HTML))
    .pipe( browserSync.reload(browsSyncOpt) );
    resolve();
  });
});

// gulp.task('reload', ()=>{
//   return new Promise( resolve => {
//     gulp.src('./index.html')
//       .pipe( browserSync.reload({stream:true}) );
//     resolve();
//   });
// });


gulp.task('watch', () => {
  return new Promise( resolve => {
    gulp.watch(PATH.HTML + '/**/*.*', gulp.series(['html']));
    // gulp.watch(PATH.INC + '/*.html', gulp.series(['fileinclude']));
    gulp.watch(PATH.ASSETS.STYLE + "/**/*.scss", gulp.series(['scss:compile']));
    gulp.watch(PATH.HTML + '/guide/*.scss', gulp.series(['scssguide:compile']));

    gulp.watch(PATH.ASSETS.SCRIPTMERGE + "/**/*.js", gulp.series(['script:bulid']));
    gulp.watch(PATH.ASSETS.SCRIPT + "/**/*.*", gulp.series(['script:copy']));

    //gulp.watch(PATH.ASSETS.LIB + "/**/*.*", gulp.series(['librarySync']));
    gulp.watch(PATH.ASSETS.IMAGES + "/**/*.*", gulp.series(['imagesSync']));
    // gulp.watch(PATH.ASSETS.FONTS + "/*.*", gulp.series(['fontsSync']));

    //guide
    // gulp.watch(PATH.GUIDE + "/**/*.*", gulp.series(['guide', 'guide:scss']));
    // gulp.watch(PATH.HTML + '/__guide_include/*.*', gulp.series(['guide:html']));

    resolve();
  });
});

gulp.task('borwserSync', () => {
  return new Promise( resolve => {
    browserSync.init({
      server: {
        baseDir: './'
      }
    }, {
      proxy: 'http://127.0.0.1:8001',
      port: 8000,
      open: false,
      notify: false,
      ghostMode: false,
    }
    );

    resolve();
  });
});

gulp.task('default', gulp.series([
  'nodemon:start',
  'imagesSync',
  'fontsSync',
  //'librarySync',
  'html',
  // 'fileinclude',
  'script:copy',
  'script:bulid',
  'scss:compile',
  'scssguide:compile',
  // 'guide',
  // 'guide:scss',
  'borwserSync', 
  'watch'
]));