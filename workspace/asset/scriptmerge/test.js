jQuery(function(){
  //datepicker
  if(!$.datepicker) return false;
	var datePicker;
	
	var dateInit = function(){
		$.datepicker.setDefaults({
			dateFormat: 'yy-mm-dd',
			prevText: '이전 달',
			nextText: '다음 달',
			monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			dayNames: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
			dayNamesShort: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
			dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
			showMonthAfterYear: true,
			changeYear: false,
			yearSuffix: '년',
		});
	}

	var dateRange = function(){
		var dateRangSet = $('.cs-datepicker-set');
		dateRangSet.each(function(){
			var row = $(this);
			var from = row.find('.js__date-from');
			var to = row.find('.js__date-to');

			// from.datepicker("setDate",new Date());
			// to.datepicker("setDate","+ 1w");

			from.datepicker({
			}).on( "change", function() {
				// console.log('나 바꼈다');
				to.datepicker( "option", "minDate", getDateRang( this ) );
			});

			to.datepicker({
			}).on( "change", function() {
				from.datepicker( "option", "maxDate", getDateRang( this ) );
			});

		});
	}

	function getDateRang( element ) {
		var date;
		try {
			date = $.datepicker.parseDate( "yy-mm-dd", element.value );
		} catch( error ) {
			date = null;
		}

		return date;
	}

	

	var init = function(){
		//datepicker 한글설정
		dateInit();

		$('.js__datepicker').datepicker({
			changeMonth: false,
			changeYear: false,
		});

		$('.modal-calendar').on('click',function(){
		});
		
		dateRange();
	}

	init();

	$('.modal-datepicker_close, .modal-datepicker_wrap').click(function(){
			$('.modal-datepicker').hide();
	});
	
	$('.datepicker').click(function(e){
			e.stopPropagation();
	});

	//date
	getDate = function(date){
		if(!date) date = new Date();
		var y = date.getFullYear();
				m = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1;
				d = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();

				return y + '-' + m + '-' + d;
	}


  //첨부파일
  inputFileSet = function(){
		var fileTarget = $('.filebox .upload-hidden');

		fileTarget.on('change', function(){
				if(window.FileReader){
						var filename = $(this)[0].files[0].name;
				} else {
						var filename = $(this).val().split('/').pop().split('\\').pop();
				}

				$(this).siblings('.upload-name').val(filename);
		});
	}
  

  //로케이션
  locationList = function(){
		var depth1 = $('.location-list > li > button');
		var speed = 200;

		$('body').on('click',function(){
			depth1.next('ul').stop().slideUp(speed);
			depth1.parent().removeClass('active');
		});

		depth1.on('click', function(e){
			e.stopPropagation();
			var li = $(this).parent();
			if(!li.hasClass('active')){
				li.addClass('active');
				$(this).next('ul').stop().slideDown(speed);
			}else{
				li.removeClass('active');
				$(this).next('ul').stop().slideUp(speed);
			}
			
		});
	}

	locationList();



  //모달
  $('body').on('keyup', function(e){
		if(e.key === "Escape") {
			lay_pop_close();
		}
	});

	var bgCloseAni;

	var bgOpen = function(){
		clearTimeout(bgCloseAni);
		var $popbg = $('#jq_modal_bg');
		$('#jq_modal_bg').removeClass('jq_modal_bg_close');

		if($popbg.length == 0){
			$('body').prepend('<div id="jq_modal_bg"></div>');
		}else{
			$popbg.css('display','block');
		}
	}

	var bgClose = function(){
		$('#jq_modal_bg').addClass('jq_modal_bg_close');

		bgCloseAni = setTimeout(function(){
			$('#jq_modal_bg').removeClass('jq_modal_bg_close');
			$('#jq_modal_bg').css('display','none');
			$('#jq_modal_bg').unbind();
			$('html').attr('style','');
		},300);
	}


	lay_pop_close = function(elm){
		var $obj;

		if(elm){
			if(typeof(elm) == 'string'){
				$obj = $(elm);
			}else{
				$obj = elm;
			}
		}else{
			$obj = $('.jq_modal');
		}

		$obj.removeClass('modal_open').addClass('modal_close');

		$carea = $('*[jq-carea='+$obj.data('jqModalarea')+']');
		$carea.focus();

		bgClose();

		setTimeout(function(){
			$obj.removeClass('modal_close');
			$obj.closest('.jq_modal_wrap').css('display','none');
			$obj.unbind();
		},300);
	}

		//lay_pop_open(button,target object, close action)
		lay_pop_open = function(obj,t,bgclose){
			// if(bgclose != false) bgclose = true;
			var $t = t; //회귀
			var $obj = obj; //mdoal
			// var $bgClose = bgclose; //배경클릭시 모달 닫기기능 활성화 여부 true: 활성, false: 비활성
			var $bgClose = false; //배경클릭시 모달 닫기기능 활성화 여부 true: 활성, false: 비활성
			
			if(!$obj){
				//$obj없을경우, href or jq_target 에 있는 정보를 사용함
				//target object
				$obj = $($t.attr('href'));
				//링크 사용한하고 jq_target으로 사용
				if($obj.length == 0)  $obj = $($t.attr('jq-target'));
			}

			if(typeof($obj) === 'string'){
				$obj = $(obj);
			}

			//modal없을때
			if($obj.length == 0) {
				console.error(obj+' 를 찾을수 없음');
				return false;
			}

			if($t){
				var $carea = $t.attr('id');

				$carea = new Date();
				$carea = $carea.getTime();
				$t.attr('jq-carea',$carea);
				$obj.data('jq-modalarea',$carea);
			} 

			//modal default setting
			$obj.data('jqModalBgclose', $bgClose);

			//background add
			bgOpen();

			isCreate = $obj.data('jq-modal') == 'create' ? true : false;
				
			if(!isCreate){
				$obj.data('jq-modal','create');
				$('body').prepend($obj);
				$obj.wrap('<div class="jq_modal_wrap"><div><div>');
				$obj.css('display','block');
				$obj.addClass('modal_open jq_modal');

				//modal에서 포커스 나갔을때 event add
				modalFocusOutClose($obj);
			}else{
				pobj = $obj.closest('.jq_modal_wrap');
				pobj.css('display','block');
				$obj.data('jq-modalarea',$carea);
				$obj.removeClass('modal_close');
				$obj.addClass('modal_open');
			}

			$obj.find('.slick-slider').resize();

			if($bgClose)
			{
				//배경클릭시 모달 닫기
				$('.jq_modal_wrap').click(function(){
						lay_pop_close();
				});
			}


			$obj.click(function(e){
				e.stopPropagation();
			});
				
			$('html').css({overflow: 'hidden'});

			$obj.attr('tabindex', '0');
			$obj.focus();
			$obj.focusout(function(){
					$obj.removeAttr('tabindex');
			});
		}

		//포커스가 나갈때 모달창 닫기
		var modalFocusOutClose = function($obj){
			var btn = $obj.find('button[jq-action=closeModalFocus]'),
					btns;
			var modal ={
				config: $obj.data()
			}

			//btn 생성
			if(btn.length < 1){
				btn = $('<button type="button" class="laymodal-focusclose" jq-action="closeModalFocus">');
				$obj.after(btn);
				$obj.before(btn.clone());
				btns = $obj.closest('.jq_modal_wrap').find('button[jq-action=closeModalFocus]');
			}

			if(modal.config.jqModalBgclose){
				btns.focusin(function(){
					lay_pop_close($(this).closest('.jq_modal_wrap').find('.jq_modal'));
				});
			}else{
				btns.focusin(function(){
					$obj.attr('tabindex','0').focus();
				});
			}
		}


		lay_pop_width = function(obj, width){
			$(obj).css('width', width);
		}


	$('*[jq-action="modal"]').click(function(e){
		e.preventDefault();
		lay_pop_open('',$(this));
	});

	$('*[jq-action="closeModal"]').click(function(e){
		e.preventDefault();
		lay_pop_close($(this).closest('.jq_modal'));
	});


  //체험등록 section toogle
  toogleList = function(){
		$('.sec-header').parent('section').addClass('on');
		$('.sec-header').on('click',function(e){
			if($(this).parent('section').hasClass('on')){
				$(this).parent('section').removeClass('on');
				$(this).next().stop().slideUp();
				$(this).find('.sec-toggle-btn').css('transform','rotate(180deg)');
			}else{
				$(this).parent('section').addClass('on');
				$(this).next().stop().slideDown();
				$(this).find('.sec-toggle-btn').css('transform','rotate(0deg)');
			}			
		});
	}

	selectList = function(){
		$('.select_list').hide();
		$('.select_ui button').on('click',function(e){
			if($(this).hasClass('on')){
				$(this).removeClass('on');
				$(this).next().stop().slideUp();
			}else{
				$(this).addClass('on');
				$(this).next().stop().slideDown();
			}			
		});
		
		$('.select_list li').on('click',function(e){
			$(this).parent().prev().removeClass('on');
			$(this).parent().stop().slideUp();		
		});
	}

	toogleList();
	selectList();
});



//모달사용을 위한함수
lay_pop_open = function(obj,t,bgclose){
	jQuery(function($){
		lay_pop_open(obj,t,bgclose);
	});
}

lay_pop_close = function(){
	jQuery(function($){
		lay_pop_close();
	});
}

lay_pop_width = function(obj, width){
	jQuery(function($){
		lay_pop_width(obj, width);
	});
}